import java.io.InputStreamReader;
import java.util.Scanner;

public class CheckPairSum {
    public static boolean PairSum(int A[], int c) {
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[i] + A[j] == c) {
                    System.out.println("Pair of sum " + c + " is (" + A[i] + ", " + A[j] + ")");
                    return true;
                }
            }
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(new InputStreamReader(System.in));
        int num = input.nextInt();
        int A[] = new int[num];

        for(int i=0;i<num;i++) {
            A[i] = input.nextInt();
        }
        int c = input.nextInt();

        PairSum(A, c);
    }
}